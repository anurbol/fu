"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
function establishConnection() {
    return new Promise(function (res, rej) {
        mongoose_1.default.connect('mongodb://localhost/nurbolAlpysbayevJobChallenge', { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongoose_1.default.connection;
        db.on('error', function () { return rej('db connection error'); });
        db.once('open', function () {
            var donationsSchema = new mongoose_1.default.Schema({
                amount: Number,
                currency: String
            });
            var Donation = mongoose_1.default.model('Donation', donationsSchema);
            res({
                models: {
                    Donation: Donation
                }
            });
        });
    });
}
exports.establishConnection = establishConnection;
