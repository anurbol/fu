import mongoose from 'mongoose';

export function establishConnection() {
    
    type Model = mongoose.Model<mongoose.Document, {}>;

    return new Promise<{models: {Donation: Model}}>((res, rej) => {
        mongoose.connect('mongodb://localhost/nurbolAlpysbayevJobChallenge', { useNewUrlParser: true, useUnifiedTopology: true });

        var db = mongoose.connection;

        db.on('error', () => rej('db connection error'));
        db.once('open', function () {
            var donationsSchema = new mongoose.Schema({
                amount: Number,
                currency: String
            });

            const Donation = mongoose.model('Donation', donationsSchema);

            res({
                models: {
                    Donation
                }
            })
        })
    })
}