import Koa from 'koa'
import Router from 'koa-router'
import bodyParser from 'koa-bodyparser'
import serve from 'koa-static'
import {establishConnection} from "./db"

establishConnection().then(({models}) => {

  console.log("connected");

  const 
    app = new Koa(),
    router = new Router();
    
  app.use(serve('client/dist'));
      
  app.use(bodyParser());

  router.post('/donate', async (ctx, next) => {
    const {amount, currency} = ctx.request.body

    const supportedCurrencies = ['USD', 'EUR', 'GBP', 'RUB']

    if (!supportedCurrencies.includes(currency)) {
      ctx.body = {
        ok: false,
        message: "Неподдерживаемая валюта"
      }
    } else {
      await new Promise(res => {
        new models.Donation({amount, currency}).save(function (err, donation) {
          if (err)  {
            ctx.body = err
          } else {
            ctx.body = {ok: true}
          }
          res()
        });
      })
    }
  });

  app
    .use(router.routes())
    .use(router.allowedMethods());

  app.listen(3000);
});


